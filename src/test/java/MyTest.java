import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class MyTest extends BaseTest {
    private final static String MY_URL = "https://ru.wikipedia.org/";
    private final static String MY_SEARCH = "Санкт-Петербург";
    @Test
    public void checkWikipediaResult() {
        MainPage mainPage = new MainPage(MY_URL);
        System.out.println("Зашли на википедию");
        mainPage.searchOnMainPage(MY_SEARCH);
        System.out.println("Осуществили поиск города Санкт-Петебург");
        ResultPage resultPage = new ResultPage();
        Assertions.assertEquals(resultPage.checkTheCountry(), "Россия");
        System.out.println("Получили страну: "+resultPage.checkTheCountry());
    }
}
