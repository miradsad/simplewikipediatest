import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;
public class ResultPage {
    private final SelenideElement country = $x("//span[@data-wikidata-property-id=\"P17\"]/span");
    public String checkTheCountry(){
        sleep(2000);
        return country.getAttribute("data-sort-value");
    }

}
