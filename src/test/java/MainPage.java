import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import static com.codeborne.selenide.Selenide.*;

public class MainPage {
    private final SelenideElement searchbar = $x("//input[@class=\"vector-search-box-input\"]");
    public MainPage(String url) {
        Selenide.open(url);
    }
    public MainPage searchOnMainPage(String mySearch){
        sleep(2000);
        searchbar.setValue(mySearch).sendKeys(Keys.ENTER);
        sleep(2000);
        return this;
    }
}
