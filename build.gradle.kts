plugins {
    id("java")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.2"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
    implementation("io.github.bonigarcia:webdrivermanager:5.6.3")
    implementation("org.testng:testng:7.7.0")
    implementation("org.junit.jupiter:junit-jupiter:5.9.2")
    implementation("junit:junit:4.13.2")
    implementation("org.seleniumhq.selenium:selenium-chrome-driver:4.11.0")
    implementation("com.codeborne:selenide:6.15.0")
}

tasks.test {
    useJUnitPlatform()
}